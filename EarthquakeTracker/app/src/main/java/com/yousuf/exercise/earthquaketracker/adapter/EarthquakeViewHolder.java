package com.yousuf.exercise.earthquaketracker.adapter;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.annotation.LayoutRes;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

/**
 * @author Yousuf Syed on 8/25/17.
 */

public class EarthquakeViewHolder<T extends ViewDataBinding> extends RecyclerView.ViewHolder {
    public final T binding;

    public EarthquakeViewHolder(T binding) {
        super(binding.getRoot());
        this.binding = binding;
    }

    /**
     * Create view for Create View holder.
     * @param parent
     * @param layoutId
     * @param <T>
     * @return
     */
    public static <T extends ViewDataBinding> EarthquakeViewHolder<T> create(
            ViewGroup parent,
            @LayoutRes int layoutId){
        T binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                layoutId, parent, false);
        return new EarthquakeViewHolder<>(binding);
    }
}
