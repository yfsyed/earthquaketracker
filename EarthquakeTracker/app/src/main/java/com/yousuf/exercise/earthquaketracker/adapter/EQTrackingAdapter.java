package com.yousuf.exercise.earthquaketracker.adapter;

import android.databinding.ViewDataBinding;
import android.support.annotation.LayoutRes;

/**
 * @author Yousuf Syed on 8/25/17.
 */

public abstract class EQTrackingAdapter<T extends ViewDataBinding> extends BaseEQTrackingAdapter<T> {

    @LayoutRes
    private final int mLayoutId;

    public EQTrackingAdapter(@LayoutRes int layoutId) {
        mLayoutId = layoutId;
    }

    @Override
    public int getItemLayoutId(int position) {
        return mLayoutId;
    }

}
