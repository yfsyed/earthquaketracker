package com.yousuf.exercise.earthquaketracker.viewmodel;

import android.arch.lifecycle.ViewModel;

import com.yousuf.exercise.earthquaketracker.EQApplication;
import com.yousuf.exercise.earthquaketracker.model.ProjectModel;
import com.yousuf.exercise.earthquaketracker.network.EarthquakeNetworkAdapter;

/**
 * @author Yousuf Syed on 8/26/17.
 */

public abstract class EarthQuakeVM<T extends ViewModel> extends ViewModel {
    public final T viewModel;

    public EarthQuakeVM() {
        this.viewModel = getVM();
    }

    public abstract T getVM();

    public EarthquakeNetworkAdapter getNetworkAdapter() {
        return EQApplication.getInstance().getNetworkAdapter();
    }
}
