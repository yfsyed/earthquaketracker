package com.yousuf.exercise.earthquaketracker.view.fragment;

import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.yousuf.exercise.earthquaketracker.BR;
import com.yousuf.exercise.earthquaketracker.R;
import com.yousuf.exercise.earthquaketracker.databinding.FragmentEarthquakeListBinding;
import com.yousuf.exercise.earthquaketracker.viewmodel.EarthquakeTrackerVM;

/**
 * A placeholder fragment containing a simple view.
 */
public class EarthquakeListFragment extends Fragment {

    public static final String TAG = EarthquakeListFragment.class.getSimpleName();
    private FragmentEarthquakeListBinding eqListBinding;
    private EarthquakeTrackerVM vm;

    public EarthquakeListFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        eqListBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_earthquake_list, container, false);
        vm = ViewModelProviders.of(this).get(EarthquakeTrackerVM.class);
        eqListBinding.setVariable(BR.actionCallBack, vm);
        return eqListBinding.getRoot();
    }
}
