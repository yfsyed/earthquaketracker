package com.yousuf.exercise.earthquaketracker.network;

import android.content.Context;

import com.yousuf.exercise.earthquaketracker.R;
import com.yousuf.exercise.earthquaketracker.binding.BindableInt;
import com.yousuf.exercise.earthquaketracker.utils.EarthquakeUtils;

import java.lang.ref.WeakReference;

import rx.Observer;

/**
 * @author Yousuf Syed on 8/25/17.
 */

public class BaseResponseObserver<T extends Object> implements Observer<T> {

    protected WeakReference<Context> weakContext;
    private BindableInt progress;

    public BaseResponseObserver(Context ctx, BindableInt progress) {
        weakContext = new WeakReference<>(ctx);
        this.progress = progress;
    }

    @Override
    public void onCompleted() {
        progress.set(0);
    }

    @Override
    public void onError(Throwable e) {
        progress.set(0);
        Context ctx = weakContext.get();
        if (ctx != null) {
            EarthquakeUtils.showInfoMessage(ctx, R.string.unable_to_fetch_data);
        }
    }

    @Override
    public void onNext(T t) {
    }
}
