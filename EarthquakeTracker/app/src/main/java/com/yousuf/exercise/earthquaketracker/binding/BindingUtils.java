package com.yousuf.exercise.earthquaketracker.binding;

import android.databinding.BindingAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.maps.MapView;
import com.yousuf.exercise.earthquaketracker.R;
import com.yousuf.exercise.earthquaketracker.adapter.EQTrackingAdapter;
import com.yousuf.exercise.earthquaketracker.factory.AdapterFactory;
import com.yousuf.exercise.earthquaketracker.model.CardItem;
import com.yousuf.exercise.earthquaketracker.model.EarthQuakeList;
import com.yousuf.exercise.earthquaketracker.model.Earthquake;
import com.yousuf.exercise.earthquaketracker.model.ProjectModel;
import com.yousuf.exercise.earthquaketracker.viewmodel.EarthquakeTrackerVM;

import java.util.List;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

/**
 * @author Yousuf Syed on 8/25/17.
 */

public class BindingUtils {

    @BindingAdapter("app:visibility")
    public static void setVisibility(View view, CardItem cardItem) {
        view.setSelected(cardItem.isSelected());
    }

    @BindingAdapter("app:text")
    public static void showView(TextView view, int resId) {
        if (resId > 0) view.setText(resId);
    }

    @BindingAdapter({"app:updateOnChange", "app:empty_view", "app:viewModel", "app:type"})
    public static void earthquakeLists(RecyclerView recyclerView,
                                       BindableInt progress, TextView emptyView,
                                       EarthquakeTrackerVM vm, AdapterFactory.Type type) {
        if (progress.get() == 1)
            return;

        emptyView.setVisibility(VISIBLE);
        List<Earthquake> list = vm.getEQList();
        if (list != null && !list.isEmpty()) {
            emptyView.setVisibility(View.GONE);
            recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));
            EQTrackingAdapter adapter = AdapterFactory.getAdapterInstance(type, vm, list);
            recyclerView.setAdapter(adapter);
        }
    }

    @BindingAdapter("app:fill_color")
    public static void setBackground(TextView view, double magnitude) {
        int color = ContextCompat.getColor(view.getContext(), R.color.primary_text);
        if (magnitude > 6.0) {
            color = ContextCompat.getColor(view.getContext(), R.color.red);
        }
        view.setTextColor(color);
    }

    @BindingAdapter({"app:resId", "app:text"})
    public static void setText(TextView view, String resString, double value) {
        view.setText((value != 0) ? String.format(resString, value) : "");
    }

    @BindingAdapter({"app:resId", "app:text"})
    public static void setText(TextView view, String resString, String value) {
        view.setText(!TextUtils.isEmpty(value) ? String.format(resString, value) : "");
    }

    @BindingAdapter({"app:viewModel", "app:data"})
    public static void setText(MapView view, EarthquakeTrackerVM vm, Earthquake data) {
        vm.showMarker(data);
    }

    @BindingAdapter({"app:binder", "app:fill_color"})
    public static void setBackground(TextView view, ProjectModel data, double magnitude) {
        int color = ContextCompat.getColor(view.getContext(), R.color.primary_text);
        if (magnitude > 6.0) {
            color = ContextCompat.getColor(view.getContext(), R.color.red);
        }
        view.setTextColor(color);
    }

    @BindingAdapter({"app:binder", "app:resId", "app:text"})
    public static void setText(TextView view, ProjectModel data, String resString, String value) {
        view.setText(!TextUtils.isEmpty(value) ? String.format(resString, value) : "");
    }

    @BindingAdapter({"app:binder", "app:text"})
    public static void setText(TextView view, ProjectModel data, String value) {
        view.setText( value );
    }
}
