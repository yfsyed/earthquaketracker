package com.yousuf.exercise.earthquaketracker.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Yousuf Syed on 8/26/17.
 */

public class EarthQuakeList {

    private ArrayList<Earthquake> earthquakes;

    public ArrayList<Earthquake> getEarthquakes() {
        return earthquakes;
    }

    public void setEarthquakes(ArrayList<Earthquake> earthquakes) {
        this.earthquakes = earthquakes;
    }

}
