package com.yousuf.exercise.earthquaketracker.network;

import com.yousuf.exercise.earthquaketracker.model.EarthQuakeList;

import java.util.List;

import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Retrofit Endpoint interface to make network calls
 *
 * @author Yousuf Syed on 8/25/17.
 */
public interface EarthQuakeListApi {

    @GET("/earthquakesJSON")
    Observable<EarthQuakeList> getEarthquakeList(
            @Query("formatted") String type,
            @Query("username") String username,
            @Query("north") String north,
            @Query("south") String south,
            @Query("east") String east,
            @Query("west") String west);

}
