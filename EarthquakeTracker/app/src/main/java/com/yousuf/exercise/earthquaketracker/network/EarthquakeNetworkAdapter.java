package com.yousuf.exercise.earthquaketracker.network;

import com.yousuf.exercise.earthquaketracker.BuildConfig;
import com.yousuf.exercise.earthquaketracker.model.EQRequestData;
import com.yousuf.exercise.earthquaketracker.model.EarthQuakeList;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.Observable;

/**
 * @author Yousuf Syed on 8/25/17.
 */
public class EarthquakeNetworkAdapter {

    private EarthQuakeListApi mApi;

    public EarthquakeNetworkAdapter() {
        Retrofit mRequest = new Retrofit.Builder()
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(BuildConfig.BASE_URL)
                .build();
        mApi = mRequest.create(EarthQuakeListApi.class);
    }

    public Observable<EarthQuakeList> getEarthQuakeList(EQRequestData eqRequestData) {
        return mApi.getEarthquakeList(eqRequestData.getType(), eqRequestData.getUsername(),
                eqRequestData.getNorth(), eqRequestData.getSouth(),
                eqRequestData.getEast(), eqRequestData.getWest());
    }
}
