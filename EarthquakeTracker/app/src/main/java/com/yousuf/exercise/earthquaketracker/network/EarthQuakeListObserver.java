package com.yousuf.exercise.earthquaketracker.network;

import android.content.Context;

import com.yousuf.exercise.earthquaketracker.binding.BindableInt;
import com.yousuf.exercise.earthquaketracker.model.EarthQuakeList;
import com.yousuf.exercise.earthquaketracker.model.Earthquake;
import com.yousuf.exercise.earthquaketracker.model.ProjectModel;

import java.util.List;

/**
 * @author Yousuf Syed on 8/25/17.
 */

public class EarthQuakeListObserver extends BaseResponseObserver<EarthQuakeList> {

    public EarthQuakeListObserver(Context ctx, BindableInt progress) {
        super(ctx, progress);
    }

    @Override
    public void onNext(EarthQuakeList earthQuakeList) {
        ProjectModel.getInstance().setEarthquakes(
                earthQuakeList != null ? earthQuakeList.getEarthquakes() : null);
    }
}