package com.yousuf.exercise.earthquaketracker.factory;

import android.databinding.ViewDataBinding;

import com.yousuf.exercise.earthquaketracker.R;
import com.yousuf.exercise.earthquaketracker.adapter.EQTrackingAdapter;
import com.yousuf.exercise.earthquaketracker.adapter.RecyclerViewAdapterImpl;

/**
 * @author Yousuf Syed on 8/26/17.
 */

public class AdapterFactory {

    public enum Type {
        EARTHQUAKE_LIST(R.layout.earthquake_item);

        private int layoutId;

        public int getLayoutId() {
            return layoutId;
        }

        Type(int layoutId) {
            this.layoutId = layoutId;
        }
    }

    public static <T extends ViewDataBinding> EQTrackingAdapter<T> getAdapterInstance(Type type, Object... data) {
        int layoutId = type.getLayoutId();
        switch (type) {
            case EARTHQUAKE_LIST:
                return new RecyclerViewAdapterImpl.EarthquakeListAdapter<T>(layoutId, data[0], data[1]);
        }
        return null;
    }
}
