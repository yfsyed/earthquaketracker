package com.yousuf.exercise.earthquaketracker.model;

import android.databinding.BaseObservable;

/**
 * Created by Intern on 8/27/17.
 */

public class CardItem extends BaseObservable {
    private boolean selected = false;

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
        notifyChange();
    }
}
