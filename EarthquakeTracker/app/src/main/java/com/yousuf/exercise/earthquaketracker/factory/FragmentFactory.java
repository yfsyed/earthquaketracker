package com.yousuf.exercise.earthquaketracker.factory;

import android.content.Context;
import android.support.v4.app.Fragment;

import com.yousuf.exercise.earthquaketracker.utils.FragmentUtils;
import com.yousuf.exercise.earthquaketracker.view.fragment.EarthquakeDetailsFragment;
import com.yousuf.exercise.earthquaketracker.view.fragment.EarthquakeListFragment;

/**
 * @author Yousuf Syed on 8/25/17.
 */
public class FragmentFactory {

    public enum Type{
        EARTHQUAKE_LIST(EarthquakeListFragment.TAG, EarthquakeListFragment.class),
        EARTHQUAKE_DETAILS(EarthquakeDetailsFragment.TAG,EarthquakeDetailsFragment.class);

        String fragmentName;
        Class className;

        Type(String name, Class className){
            this.className = className;
            this.fragmentName = name;
        }

        public Class getClassName(){
            return className;
        }

        public String getFragmentName(){
            return fragmentName;
        }
    }

    public static void showFragmentByType(Context ctx, int layoutId, Type type, boolean backstack) {
        if (type == null)
            return;
        FragmentUtils.showFragmentWithType(ctx, type, layoutId, backstack);
    }

    public static Fragment getFragmentClass(Type type) throws
            InstantiationException, IllegalAccessException {
        return (Fragment) type.getClassName().newInstance();
    }
}
