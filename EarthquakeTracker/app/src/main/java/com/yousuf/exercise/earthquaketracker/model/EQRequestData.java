package com.yousuf.exercise.earthquaketracker.model;

/**
 * @author Yousuf Syed on 8/26/17.
 */

//"true", "", "44.1", "-9.9", "-22.4", "55.2"
public class EQRequestData {

    private String type = "true";
    private String username ="mkoppelman";
    private String north = "44.1";
    private String south = "-9.9";
    private String east = "-22.4";
    private String west = "55.2";

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getNorth() {
        return north;
    }

    public void setNorth(String north) {
        this.north = north;
    }

    public String getSouth() {
        return south;
    }

    public void setSouth(String south) {
        this.south = south;
    }

    public String getEast() {
        return east;
    }

    public void setEast(String east) {
        this.east = east;
    }

    public String getWest() {
        return west;
    }

    public void setWest(String west) {
        this.west = west;
    }
}
