package com.yousuf.exercise.earthquaketracker.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.TypedArray;
import android.net.Uri;
import android.os.IBinder;
import android.support.annotation.MainThread;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

/**
 * @author Yousuf Syed on 8/25/17.
 */

public class EarthquakeUtils {

    @MainThread
    public static void showDialog(Context ctx, int messageId) {
        (new AlertDialog.Builder(ctx)
                .setMessage(messageId)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                }).create()).show();
    }

    @MainThread
    public static void showDialog(Context ctx, int messageId,
                                  DialogInterface.OnClickListener positive,
                                  DialogInterface.OnClickListener negative) {
        (new AlertDialog.Builder(ctx)
                .setMessage(messageId)
                .setPositiveButton(android.R.string.ok, positive)
                .setNegativeButton(android.R.string.cancel, negative)
                .create()).show();
    }

    @MainThread
    public static void showIndefiniteMessage(Context ctx, int msgId,
                                             int actionTitleId, View.OnClickListener listener) {
        Snackbar.make(((Activity) ctx).findViewById(android.R.id.content),
                msgId, Snackbar.LENGTH_INDEFINITE)
                .setAction(actionTitleId, listener).show();
    }

    @MainThread
    public static void showInfoMessage(Context ctx, int msgId) {
        Snackbar.make(
                ((Activity) ctx).findViewById(android.R.id.content),
                msgId, Snackbar.LENGTH_SHORT).show();
    }

    @MainThread
    public static void showInfoMessage(Context ctx, String msg) {
        Snackbar.make(
                ((Activity) ctx).findViewById(android.R.id.content),
                msg, Snackbar.LENGTH_SHORT).show();
    }

    @MainThread
    public static void dismissKeyboard(Context ctx, IBinder binder) {
        InputMethodManager imm = (InputMethodManager) ctx.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(binder, 0);
    }

    @MainThread
    public static void closeOnTransition(Context ctx) {
        ((Activity) ctx).finish();
        ((Activity) ctx).overridePendingTransition(0, 0);
    }

    public static String getString(Context ctx, int resId) {
        if (resId > 0) {
            return ctx.getString(resId);
        }
        return "";
    }
}
