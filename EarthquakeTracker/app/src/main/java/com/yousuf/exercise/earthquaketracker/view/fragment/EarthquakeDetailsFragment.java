package com.yousuf.exercise.earthquaketracker.view.fragment;

import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.yousuf.exercise.earthquaketracker.BR;
import com.yousuf.exercise.earthquaketracker.R;
import com.yousuf.exercise.earthquaketracker.databinding.FragmentEarthquakeDetailsBinding;
import com.yousuf.exercise.earthquaketracker.databinding.FragmentEarthquakeListBinding;
import com.yousuf.exercise.earthquaketracker.model.ProjectModel;
import com.yousuf.exercise.earthquaketracker.viewmodel.EarthquakeTrackerVM;

/**
 * A placeholder fragment containing a simple view.
 */
public class EarthquakeDetailsFragment extends Fragment implements OnMapReadyCallback {

    public static final String TAG = EarthquakeDetailsFragment.class.getSimpleName();
    private FragmentEarthquakeDetailsBinding eqDetailsBinding;
    private EarthquakeTrackerVM vm;

    public EarthquakeDetailsFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        eqDetailsBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_earthquake_details, container, false);
        vm = ViewModelProviders.of(this).get(EarthquakeTrackerVM.class);
        vm.init();
        eqDetailsBinding.setActionCallBack(vm);
        eqDetailsBinding.setProject(ProjectModel.getInstance());
        eqDetailsBinding.map.onCreate(savedInstanceState);
        return eqDetailsBinding.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (eqDetailsBinding.map != null) {
            eqDetailsBinding.map.onResume();
            eqDetailsBinding.map.getMapAsync(this);
        }
    }

    @Override
    public void onPause() {
        if (eqDetailsBinding != null && eqDetailsBinding.map != null) {
            eqDetailsBinding.map.onPause();
        }
        super.onPause();
    }

    @Override
    public void onDestroy() {
        if (eqDetailsBinding != null && eqDetailsBinding.map != null) {
            try {
                eqDetailsBinding.map.onDestroy();
            } catch (NullPointerException e) {
                Log.e(TAG, "Error while attempting MapView.onDestroy(), ignoring exception", e);
            }
        }
        super.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        if (eqDetailsBinding != null && eqDetailsBinding.map != null) {
            eqDetailsBinding.map.onLowMemory();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (eqDetailsBinding != null && eqDetailsBinding.map != null) {
            eqDetailsBinding.map.onSaveInstanceState(outState);
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        MapsInitializer.initialize(getContext());
        googleMap.animateCamera(CameraUpdateFactory.zoomIn());
        if (vm != null) {
            vm.setGoogleMap(googleMap);
        }
    }
}
