package com.yousuf.exercise.earthquaketracker.adapter;

import android.databinding.OnRebindCallback;
import android.databinding.ViewDataBinding;
import android.support.annotation.CallSuper;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import java.util.List;

/**
 * @author Yousuf Syed on 8/25/17.
 */

public abstract class BaseEQTrackingAdapter<T extends ViewDataBinding> extends
        RecyclerView.Adapter<EarthquakeViewHolder<T>> {

    private static final Object Doorways_Data = new Object();

    public BaseEQTrackingAdapter() {
    }

    @Nullable
    private RecyclerView mRecyclerView;

    private final OnRebindCallback onRebindCallback = new OnRebindCallback() {
        @Override
        public boolean onPreBind(ViewDataBinding binding) {
            if (mRecyclerView == null || mRecyclerView.isComputingLayout()) {
                return true;
            }
            int childPosition = mRecyclerView.getChildAdapterPosition(binding.getRoot());
            if (childPosition == RecyclerView.NO_POSITION) {
                return true;
            }
            notifyItemChanged(childPosition, Doorways_Data);
            return false;
        }
    };

    @Override
    @CallSuper
    public EarthquakeViewHolder<T> onCreateViewHolder(ViewGroup parent, int viewType) {
        EarthquakeViewHolder<T> viewHolder = EarthquakeViewHolder.create(parent, viewType);
        viewHolder.binding.addOnRebindCallback(onRebindCallback);
        return viewHolder;
    }

    @Override
    public final void onBindViewHolder(EarthquakeViewHolder<T> holder, int position,
                                       List<Object> data) {
        if (data.isEmpty() || hasNonDataBindingInvalidate(data)) {
            bindItem(holder, position, data);
        }
        holder.binding.executePendingBindings();
    }

    private boolean hasNonDataBindingInvalidate(List<Object> dataSet) {
        for (Object data : dataSet) {
            if (data != Doorways_Data) {
                return true;
            }
        }
        return false;
    }

    @Override
    public final void onBindViewHolder(EarthquakeViewHolder<T> holder, int position) {
        throw new IllegalArgumentException("just overridden to make final.");
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        mRecyclerView = recyclerView;
    }

    @Override
    public void onDetachedFromRecyclerView(RecyclerView recyclerView) {
        mRecyclerView = null;
    }

    @Override
    public final int getItemViewType(int position) {
        return getItemLayoutId(position);
    }

    protected abstract void bindItem(EarthquakeViewHolder<T> holder, int position,
                                     List<Object> payloads);

    @LayoutRes
    abstract public int getItemLayoutId(int position);

}
