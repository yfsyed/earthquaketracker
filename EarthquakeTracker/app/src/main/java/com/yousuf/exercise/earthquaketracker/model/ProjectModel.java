package com.yousuf.exercise.earthquaketracker.model;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import com.yousuf.exercise.earthquaketracker.BR;

import java.util.ArrayList;

/**
 * @author Yousuf Syed on 8/26/17.
 */

public class ProjectModel extends BaseObservable {

    private static ProjectModel sInstance;

    static {
        sInstance = new ProjectModel();
    }

    private ProjectModel() {
    }

    private Earthquake selectedEQ;

    private ArrayList<Earthquake> eqList;

    private EQRequestData requestData;

    public EQRequestData getRequestData() {
        if (requestData == null) {
            requestData = new EQRequestData();
        }
        return requestData;
    }

    public static ProjectModel getInstance() {
        return sInstance;
    }

    public ArrayList<Earthquake> getEarthquakes() {
        if (eqList == null) {
            eqList = new ArrayList<>();
        }
        return eqList;
    }

    public void setEarthquakes(ArrayList<Earthquake> earthquakes) {
        eqList = earthquakes;
        if (eqList != null && eqList.size() > 0) {
            selectedEQ = getEarthquakes().get(0);
        }
        notifyChange();
    }

    public void setSelectedEarthquake(Earthquake earthquake) {
        selectedEQ = earthquake;
        notifyChange();
    }

    @Bindable
    public Earthquake getSelectedEQ() {
        return selectedEQ;
    }
}
