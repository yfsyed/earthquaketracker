package com.yousuf.exercise.earthquaketracker.view.activity;

import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;

import com.yousuf.exercise.earthquaketracker.R;
import com.yousuf.exercise.earthquaketracker.databinding.ActivityEarthquakeTrackerBinding;
import com.yousuf.exercise.earthquaketracker.factory.FragmentFactory;
import com.yousuf.exercise.earthquaketracker.model.Earthquake;
import com.yousuf.exercise.earthquaketracker.model.ProjectModel;
import com.yousuf.exercise.earthquaketracker.utils.EarthquakeUtils;
import com.yousuf.exercise.earthquaketracker.utils.FragmentUtils;
import com.yousuf.exercise.earthquaketracker.viewmodel.EarthquakeTrackerVM;

import java.util.ArrayList;

public class EarthquakeTrackerActivity extends AppCompatActivity {

    private static final String EXTRA_SELECTED_EARTHQUAKE ="com.yousuf.exercise.earthquaketracker.SELECTED_EARTHQUAKE";
    private static final String EXTRA_EARTHQUAKE_LIST ="com.yousuf.exercise.earthquaketracker.EARTHQUAKE_LIST";

    private ActivityEarthquakeTrackerBinding eqTrackerBinding;
    private EarthquakeTrackerVM eqTrackerVM;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        eqTrackerBinding = DataBindingUtil.setContentView(this, R.layout.activity_earthquake_tracker);
        setSupportActionBar(eqTrackerBinding.toolbar);
        //setup VM
        boolean isLandScape = getResources().getBoolean(R.bool.isLandscape);
        eqTrackerVM = ViewModelProviders.of(this).get(EarthquakeTrackerVM.class);
        eqTrackerVM.setLandscape(isLandScape);
        eqTrackerBinding.setActionCallBack(eqTrackerVM);
        //setup fragment.
        if (savedInstanceState == null) {
            FragmentFactory.showFragmentByType(EarthquakeTrackerActivity.this,
                    R.id.fragment_list_container, FragmentFactory.Type.EARTHQUAKE_LIST, true);
            //Assuming we have the data required for the network call.
            //currently hardcoded for simplicity.
            eqTrackerVM.fetchEarthquakes(EarthquakeTrackerActivity.this);
        } else {
            ArrayList<Earthquake> earthquakeList = savedInstanceState.getParcelableArrayList(EXTRA_EARTHQUAKE_LIST);
            Earthquake selectedEarthquake = savedInstanceState.getParcelable(EXTRA_SELECTED_EARTHQUAKE);
            if(earthquakeList != null){
                ProjectModel.getInstance().setEarthquakes(earthquakeList);
            }
            if(selectedEarthquake != null){
                ProjectModel.getInstance().setSelectedEarthquake(selectedEarthquake);
            }
            if(FragmentUtils.getCount(getSupportFragmentManager())>1){
                getSupportFragmentManager().popBackStackImmediate();
            }
        }
        if (isLandScape) {
            FragmentFactory.showFragmentByType(EarthquakeTrackerActivity.this,
                    R.id.fragment_details_container, FragmentFactory.Type.EARTHQUAKE_DETAILS, false);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_earthquake_tracker, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        final int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                performBackNavigation();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        performBackNavigation();
    }

    // perform network request on
    public void onRefreshMenu(MenuItem item) {
        //Assuming we have the data required for the network call.
        //currently hardcoded for simplicity.
        eqTrackerVM.fetchEarthquakes(EarthquakeTrackerActivity.this);
    }

    //perform back navigation
    public void performBackNavigation() {
        FragmentManager fm = getSupportFragmentManager();
        if (FragmentUtils.getCount(fm) > 1) {
            fm.popBackStackImmediate();
        } else {
            EarthquakeUtils.closeOnTransition(this);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelableArrayList(EXTRA_EARTHQUAKE_LIST, ProjectModel.getInstance().getEarthquakes());
        outState.putParcelable(EXTRA_SELECTED_EARTHQUAKE, ProjectModel.getInstance().getSelectedEQ());
    }

}
