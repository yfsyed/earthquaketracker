package com.yousuf.exercise.earthquaketracker.adapter;

import android.databinding.ViewDataBinding;

import com.yousuf.exercise.earthquaketracker.model.EarthQuakeList;
import com.yousuf.exercise.earthquaketracker.model.Earthquake;
import com.yousuf.exercise.earthquaketracker.viewmodel.EarthquakeTrackerVM;

import java.util.List;

/**
 * @author Yousuf Syed on 8/25/17.
 */

public class RecyclerViewAdapterImpl {

    /**
     * Show Earthquake list adapter for list of earthquakes.
     */
    public static class EarthquakeListAdapter<T extends ViewDataBinding> extends
            AbstractRecyclerAdapter<T, EarthquakeTrackerVM, Earthquake> {

        public EarthquakeListAdapter(int layoutId, Object callbackVM, Object doorList) {
            super(layoutId, (EarthquakeTrackerVM) callbackVM, (List<Earthquake>) doorList);
        }
    }

}
