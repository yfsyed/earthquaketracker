package com.yousuf.exercise.earthquaketracker;

import android.app.Application;

import com.yousuf.exercise.earthquaketracker.network.EarthquakeNetworkAdapter;

/**
 * @author Yousuf Syed on 8/26/17.
 */

public class EQApplication extends Application {

    private static EQApplication sInstance;
    private EarthquakeNetworkAdapter networkAdapter;

    @Override
    public void onCreate() {
        super.onCreate();
        sInstance = this;
        networkAdapter = new EarthquakeNetworkAdapter();
    }

    public static EQApplication getInstance(){
        return sInstance;
    }

    public EarthquakeNetworkAdapter getNetworkAdapter(){
        return networkAdapter;
    }
}
