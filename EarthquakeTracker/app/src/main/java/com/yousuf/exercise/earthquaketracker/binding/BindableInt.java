package com.yousuf.exercise.earthquaketracker.binding;

import android.databinding.BaseObservable;

/**
 * @author Yousuf Syed on 8/25/17.
 */

public class BindableInt extends BaseObservable {
    private Integer value = 0;

    public Integer get() {
        return value;
    }

    public void set(Integer num) {
        if (!num.equals(value)) {
            value = num;
            notifyChange();
        }
    }
}
