package com.yousuf.exercise.earthquaketracker.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * @author Yousuf Syed on 8/26/17.
 */

public class Earthquake extends CardItem implements Parcelable {
    public Earthquake() {
    }

    @SerializedName("datetime")
    private String datetime;

    @SerializedName("depth")
    private double depth;

    @SerializedName("src")
    private String source;

    @SerializedName("eqid")
    private String earthquakeId;

    @SerializedName("magnitude")
    private double magnitude;

    @SerializedName("lat")
    private double latitude;

    @SerializedName("lng")
    private double longitude;

    public String getDatetime() {
        return datetime;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

    public double getDepth() {
        return depth;
    }

    public void setDepth(double depth) {
        this.depth = depth;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double lng) {
        this.longitude = lng;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String src) {
        this.source = src;
    }

    public String getEarthquakeId() {
        return earthquakeId;
    }

    public void setEarthquakeId(String eqid) {
        this.earthquakeId = eqid;
    }

    public double getMagnitude() {
        return magnitude;
    }

    public void setMagnitude(double magnitude) {
        this.magnitude = magnitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double lat) {
        this.latitude = lat;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.datetime);
        dest.writeDouble(this.depth);
        dest.writeString(this.source);
        dest.writeString(this.earthquakeId);
        dest.writeDouble(this.magnitude);
        dest.writeDouble(this.latitude);
        dest.writeDouble(this.longitude);
    }

    protected Earthquake(Parcel in) {
        this.datetime = in.readString();
        this.depth = in.readDouble();
        this.source = in.readString();
        this.earthquakeId = in.readString();
        this.magnitude = in.readDouble();
        this.latitude = in.readDouble();
        this.longitude = in.readDouble();
    }

    public static final Parcelable.Creator<Earthquake> CREATOR = new Parcelable.Creator<Earthquake>() {
        @Override
        public Earthquake createFromParcel(Parcel source) {
            return new Earthquake(source);
        }

        @Override
        public Earthquake[] newArray(int size) {
            return new Earthquake[size];
        }
    };

    public String toString(){
        StringBuilder sb = new StringBuilder();
        sb.append("Depth: ").append(depth).append("\n");
        sb.append("Latitude: ").append(latitude).append("\n");
        sb.append("Longitude: ").append(longitude);
        return sb.toString();
    }
}
