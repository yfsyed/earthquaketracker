package com.yousuf.exercise.earthquaketracker.utils;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;

import com.yousuf.exercise.earthquaketracker.factory.FragmentFactory;

/**
 * @author Yousuf Syed on 8/25/17.
 */

public class FragmentUtils {

    public static void showFragmentWithType(Context ctx, FragmentFactory.Type type,
                                            int containerId, boolean backstack) {
        try {
            gotoFragmentWithType(type, getFragmentManager(ctx), containerId, backstack);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        }
    }

    public static void gotoFragmentWithType(FragmentFactory.Type fragmentType,
                                            FragmentManager fm, int containerId, boolean backstack)
            throws InstantiationException, IllegalAccessException {
        if (fragmentType == null) {
            return;
        }
        Fragment fragment = fm.findFragmentByTag(fragmentType.getFragmentName());
        if (fragment == null) {
            fragment = FragmentFactory.getFragmentClass(fragmentType);
        }
        if (backstack) {
            addFragment(fm, containerId, fragment, fragmentType.getFragmentName());
        } else {
            addFragmentWithOutBackStack(fm, containerId, fragment);
        }
    }

    public static void addFragment(FragmentManager fm, int containerId,
                                   Fragment fragment, String name) {
        fm.beginTransaction()
                .replace(containerId, fragment)
                .addToBackStack(name)
                .setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
                .commit();
    }

    public static void addFragmentWithOutBackStack(FragmentManager fm, int containerId, Fragment fragment) {
        fm.beginTransaction()
                .replace(containerId, fragment)
                .setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
                .commit();
    }

    public static FragmentManager getFragmentManager(Context ctx) {
        return ((AppCompatActivity) ctx).getSupportFragmentManager();
    }

    public static int getCount(FragmentManager fm) {
        return fm.getBackStackEntryCount();
    }
}
