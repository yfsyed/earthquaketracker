package com.yousuf.exercise.earthquaketracker.binding;

import android.databinding.BaseObservable;

/**
 * @author Yousuf Syed on 8/25/17.
 */

public class BindableBoolean extends BaseObservable {
    private Boolean value = false;

    public Boolean get() {
        return value;
    }

    public void set(Boolean num) {
        if (!num.equals(value)) {
            value = num;
            notifyChange();
        }
    }
}
