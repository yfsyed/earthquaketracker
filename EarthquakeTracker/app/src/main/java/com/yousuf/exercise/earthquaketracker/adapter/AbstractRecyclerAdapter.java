package com.yousuf.exercise.earthquaketracker.adapter;

import android.databinding.ViewDataBinding;

import com.yousuf.exercise.earthquaketracker.BR;
import com.yousuf.exercise.earthquaketracker.viewmodel.EarthQuakeVM;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Yousuf Syed on 8/25/17.
 */
public abstract class AbstractRecyclerAdapter<T extends ViewDataBinding,
        V extends EarthQuakeVM, K extends Object>
        extends EQTrackingAdapter<T> {

    private List<K> mDataList = new ArrayList<>();
    private EarthQuakeVM<V> mCallbackVM;

    public AbstractRecyclerAdapter(int layoutId, EarthQuakeVM<V> callbackVM, List<K> data) {
        super(layoutId);
        mDataList.clear();
        mDataList = data;
        mCallbackVM = callbackVM;
    }

    @Override
    protected void bindItem(EarthquakeViewHolder<T> holder, int position, List<Object> payloads) {
        holder.binding.setVariable(BR.actionCallBack, mCallbackVM.getVM());
        holder.binding.setVariable(BR.data, getItem(position));
    }

    @Override
    public int getItemCount() {
        return mDataList.size();
    }

    protected K getItem(int position){
        return mDataList.get(position);
    }
}
