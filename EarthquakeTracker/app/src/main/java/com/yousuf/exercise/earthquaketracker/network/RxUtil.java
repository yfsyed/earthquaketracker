package com.yousuf.exercise.earthquaketracker.network;

import android.content.Context;

import com.yousuf.exercise.earthquaketracker.binding.BindableInt;
import com.yousuf.exercise.earthquaketracker.model.EQRequestData;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * @author Yousuf Syed on 8/25/17.
 */

public class RxUtil {
    /**
     *
     * @param ctx            Activity context to update error responses.
     * @param progress       Bindable int that helps in displaying progress update.
     * @param networkAdapter Network adapter to talk to retrofit interface.
     */
    public static void fetchEarthquakeList(
            Context ctx, EQRequestData eqRequestData,
            BindableInt progress,
            EarthquakeNetworkAdapter networkAdapter) {
        progress.set(1);
        networkAdapter.getEarthQuakeList(eqRequestData)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new EarthQuakeListObserver(ctx, progress));
    }
}
