package com.yousuf.exercise.earthquaketracker.viewmodel;

import android.arch.lifecycle.ViewModel;
import android.content.Context;
import android.view.View;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.yousuf.exercise.earthquaketracker.R;
import com.yousuf.exercise.earthquaketracker.binding.BindableBoolean;
import com.yousuf.exercise.earthquaketracker.binding.BindableInt;
import com.yousuf.exercise.earthquaketracker.factory.AdapterFactory;
import com.yousuf.exercise.earthquaketracker.factory.FragmentFactory;
import com.yousuf.exercise.earthquaketracker.model.EQRequestData;
import com.yousuf.exercise.earthquaketracker.model.Earthquake;
import com.yousuf.exercise.earthquaketracker.model.ProjectModel;
import com.yousuf.exercise.earthquaketracker.network.RxUtil;

import java.util.List;

/**
 * @author Yousuf Syed on 8/26/17.
 */

public class EarthquakeTrackerVM extends EarthQuakeVM {

    public static BindableInt progress = new BindableInt();
    public static BindableBoolean isLandscape = new BindableBoolean();
    private Earthquake currentSelection;
    private GoogleMap mGoogleMap;
    private Marker marker = null;

    public void init() {
        if (currentSelection == null) {
            currentSelection = ProjectModel.getInstance().getSelectedEQ();
        }
    }

    public void setLandscape(boolean landscape) {
        isLandscape.set(landscape);
    }

    public void setGoogleMap(GoogleMap googleMap) {
        mGoogleMap = googleMap;
        updateMarker(currentSelection);
    }

    public List<Earthquake> getEQList() {
        return ProjectModel.getInstance().getEarthquakes();
    }

    public void onEarthquakeSelected(View view, Earthquake earthquake) {
        if (earthquake != currentSelection) {
            if (currentSelection != null) currentSelection.setSelected(false);
            if (earthquake != null) earthquake.setSelected(true);
            currentSelection = earthquake;
            ProjectModel.getInstance().setSelectedEarthquake(earthquake);
            showDetailsFragment(view.getContext());
        }
    }

    public void fetchEarthquakes(Context ctx) {
        if (progress.get() == 0) {
            EQRequestData requestData = ProjectModel.getInstance().getRequestData();
            RxUtil.fetchEarthquakeList(ctx, requestData, progress, getNetworkAdapter());
        }
    }

    public void showMarker(Earthquake data) {
        currentSelection = data;
        updateMarker(data);
    }

    public AdapterFactory.Type getEQListType() {
        return AdapterFactory.Type.EARTHQUAKE_LIST;
    }

    @Override
    public ViewModel getVM() {
        return this;
    }

    private void updateMarker(Earthquake data) {
        if (marker != null) {
            marker.remove();
        }
        if (mGoogleMap != null && data != null) {
            LatLng location = new LatLng(data.getLatitude(), data.getLongitude());
            marker = mGoogleMap.addMarker(new MarkerOptions().position(location)
                    .title("Source: " + data.getSource())
                    .snippet("Depth: " + data.getDepth()));
            mGoogleMap.moveCamera(CameraUpdateFactory.newLatLng(location));
        }
    }

    private void showDetailsFragment(Context ctx) {
        if (!isLandscape.get()) {
            FragmentFactory.showFragmentByType(ctx,
                    R.id.fragment_list_container,
                    FragmentFactory.Type.EARTHQUAKE_DETAILS, true);
        }
    }

}
