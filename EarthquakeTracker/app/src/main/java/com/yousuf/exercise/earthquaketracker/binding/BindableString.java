package com.yousuf.exercise.earthquaketracker.binding;

import android.databinding.BaseObservable;
import android.text.TextUtils;

/**
 * @author Yousuf Syed on 8/25/17.
 */

public class BindableString extends BaseObservable {
    private String value;

    public String get() {
        return value != null ? value : "";
    }

    public void set(String txt) {
        if (isEmpty() || !value.equals(txt)) {
            value = txt;
            notifyChange();
        }
    }

    public boolean isEmpty() {
        return TextUtils.isEmpty(value);
    }
}
